package ru.vkopyl.kafka.consumer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;
import ru.vkopyl.kafka.model.CustomData;

@Service
public class Consumer {

    private final Logger logger = LoggerFactory.getLogger(Consumer.class);

    @KafkaListener(topics = "${spring.kafka.template.default-topic}",
                   containerFactory = "kafkaListenerContainerFactory")
    public void receive(@Payload CustomData data,
                        @Header(KafkaHeaders.RECEIVED_MESSAGE_KEY) String id){
        messageParse(data, id);
    }

    private void messageParse(CustomData data, String id){
        ObjectMapper mapper = new ObjectMapper();
        ObjectNode result = mapper.createObjectNode();
        result.put("id", id);
        result.put("name", data.getName());
        result.put("multipliedSum", Integer.toString(data.getSum()*data.getMultiplier()));
        logger.debug(result.toString());
    }
}
