package ru.vkopyl.kafka.model;

public class CustomData {
    
    private String name;
    
    private int sum;
    
    private int multiplier;
    

    public int getMultiplier() {
        return multiplier;
    }

    public void setMultiplier(int multiplier) {
        this.multiplier = multiplier;
    }

    public int getSum() {
        return sum;
    }

    public void setSum(int sum) {
        this.sum = sum;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CustomData(String name, int sum, int multiplier) {
        this.name = name;
        this.sum = sum;
        this.multiplier = multiplier;
    }
    
    public CustomData(){
    }

    @Override
    public String toString(){
        return ('{' + "\"name\":" + '\"' + name + '\"' + ','
                + "\"sum\":" + sum + ','
                + "\"multiplier\":" + multiplier + '}');
    }
}
