# Тестовое задание

# Описание

REST API в пакете producer принимает на вход PUT запрос с JSON в теле запроса.
JSON вида:

{

  "name":"test",
  
  "sum":1,
  
  "multiplier":4
  
}


где name = String, sum и multiplier = int

# Сборка

## Запуск zookeeper + kafka
Из корня проекта выполнить

```docker-compose up```

## Запуск kafka producer
Из директории /producer выполнить:

1.```mvn clean install```

2.```mvn spring-boot:run```

## Запуск kafka consumer
Из директории /consumer выполнить:

1.```mvn clean install```

2.```mvn spring-boot:run```

# Конфигурация

1. Конфигурация zookeeper+kafka находится в файле docker-compose.yml
2. Конфигурация producer находится в producer/src/main/resources/application.yml
3. Конфигурация consumer находится в consumer/src/main/resources.application.yml

По умолчанию REST API слушает на localhost:9000