package ru.vkopyl.kafka.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.vkopyl.kafka.model.CustomData;
import ru.vkopyl.kafka.producer.Producer;

@RestController
@RequestMapping("test/")
public class Controller {

    private final Producer producer;

    Controller(Producer producer){
        this.producer = producer;
    }

    @PutMapping(value = "/{uuid}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> checkId(@RequestBody CustomData message, @PathVariable String uuid){
        if(uuid.matches("^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-4[0-9a-fA-F]{3}-[89ABab][0-9a-fA-F]{3}-[0-9a-fA-F]{12}$")){
            producer.send(message, uuid);
            return new ResponseEntity<>("OK", HttpStatus.OK);
        }
        else return new ResponseEntity<>("Invalid UUID", HttpStatus.BAD_REQUEST);
    }
}
