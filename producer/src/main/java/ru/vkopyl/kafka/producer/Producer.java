package ru.vkopyl.kafka.producer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;
import ru.vkopyl.kafka.model.CustomData;

@Service
public class Producer {

    @Value("${spring.kafka.template.default-topic}")
    public String topic;

    private final Logger logger = LoggerFactory.getLogger(Producer.class);

    private KafkaTemplate<String, Message> kafkaTemplate;

    public Producer(KafkaTemplate<String, Message> kafkaTemplate) {
        this.kafkaTemplate = kafkaTemplate;
    }

    public void send(CustomData data, String uuid) {
        Message<CustomData> message = MessageBuilder
                                    .withPayload(data)
                                    .setHeader(KafkaHeaders.MESSAGE_KEY, uuid)
                                    .setHeader(KafkaHeaders.TOPIC, topic)
                                    .setHeader(KafkaHeaders.RECEIVED_TOPIC, topic)
                                    .build();
        kafkaTemplate.send(message);
        logger.debug("Producer success");
    }
}
